#language: pt

Funcionalidade: Movimentação de Contas

Contexto: Logar no site
    Dado Acessando a página de login
    E Preencho o usuário e senha e entro no site


    Cenário: 01- Inserindo uma movimentação
        Dado Cadastro uma conta "Teste Movimentação"
        E clico no ícone movimentação
        Quando preencho o campo descrição "Campo Descrição"
        E o campo valor "135"
        E o campo interessado "Nada"
        E seleciona a conta "Teste Movimentação" no combobox
        E confirmo a movimentação
        E clico no botão salvar da tela de movimentação
        Então deve apresentar a mensagem "Movimentação inserida com sucesso"
        E o valor "135" deve apresentar no grid

    # Cenário: 02- Validando valor inserido na tela inicial        
    #     Quando acessar a tela inicial de login
    #     Então o valor "135,00" deve estar presenta na tela

    Cenário: 03- Remover Movimentação
        Dado Que acesso a tela de movimentações
        Quando excluir a movimentação com o nome "Teste Movimentação"
        Então deve apresentar a mensagem "Movimentação removida com sucesso!"


    Cenário: Excluindo os dados inseridos
        Dado Clico no botão Resetar
