#language: pt

Funcionalidade: Criar contas

Contexto: Logar no site
    Dado Acessando a página de login
    E Preencho o usuário e senha e entro no site



    Cenário: 01- Cadastro de Conta  com sucesso       
        Quando Cadastro uma conta "Conta de Teste"
        Então deve apresentar a mensagem " inserida com sucesso!"


    Cenário: 02- Alterar Conta de Teste
        Dado que acesso o menu de conta
        E e clico no botão para a alterar a conta "Conta de Teste"
        E limpo o campo nome
        Quando alterar o nome da conta para "Conta de Teste Alterada"
        E clico no botão salvar
        E deve apresentar mensagem "retutdrytd"
        Então a conta deve apresentar no grid com o nome alterado "Conta de Teste Alterada"

    # Cenário: 03- Inserir uma conta já existente
    #     Dado Cadastro uma conta "Conta já inserida"
    #     Quando Cadastro uma conta "Conta já inserida"
    #     Então deve apresentar a mensagem "Error: Request failed"    

    
    Cenário: Excluindo os dados inseridos
        Dado Clico no botão Resetar

