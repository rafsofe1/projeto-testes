/// <reference types="cypress" />

import { Given, When, Then, And, then} from 'cypress-cucumber-preprocessor/steps'
import '../login/loginStep'
import '../criarConta/criarContaStep'
import moviContaPage from '../../../support/Pages/Primeiro_Projeto/moviContaPage'

And('clico no ícone movimentação', () => {   
    moviContaPage.botaoMovimentacao()
        
})

When('preencho o campo descrição {string}', (descricao) => {   
    moviContaPage.campoDesc(descricao)
        
})

And('o campo valor {string}', (valor) => {   
    moviContaPage.campoValor(valor)
        
})


And('o campo interessado {string}', (descr) => {   
    moviContaPage.campoInteressado(descr)
    
})

And('seleciona a conta {string} no combobox', (conta) => {   
    moviContaPage.comboboxConta(conta)
        
})

And('clico no botão salvar da tela de movimentação', () => {   
    moviContaPage.botaoSalvarTelaMov()
        
})


And('confirmo a movimentação', () => {   
    moviContaPage.confirmarMovi()
        
})

Then('deve apresentar a mensagem {string}', (mensagem) => {   
    moviContaPage.msgMoviInserida(mensagem)
        
})

And('o valor {string} deve apresentar no grid', (valor) => {   
    moviContaPage.validValorMov(valor)
        
})

When('acessar a tela inicial de login', () => {
    moviContaPage.home()
})

Then('o valor {string} deve estar presenta na tela', (valor) => {
    moviContaPage.validValorMov(valor)
})

Given('Que acesso a tela de movimentações', () => {
    moviContaPage.telaMovi()
})

When('excluir a movimentação com o nome {string}', (nome) => {
    moviContaPage.nomeMovi(nome)
})

Then('deve apresentar a mensagem {string}', (nome) => {
    moviContaPage.msgMovExcl(nome)
})









