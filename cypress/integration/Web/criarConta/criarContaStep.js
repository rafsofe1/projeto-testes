/// <reference types="cypress" />

import { Given, When, Then, And, then} from 'cypress-cucumber-preprocessor/steps'
import '../login/loginStep'
import CriarContaPage from '../../../support/Pages/Primeiro_Projeto/criarContaPage'


When('Cadastro uma conta {string}', (nomeConta) => {    
    CriarContaPage.menuConta()
    CriarContaPage.nomeConta(nomeConta)
    
        
})

Then('deve apresentar a mensagem {string}', (mensagem) => {
    CriarContaPage.mensagemContaGravada(mensagem)
})

Given('que acesso o menu de conta', () => {   
    CriarContaPage.menuConta() 
        
})

And('e clico no botão para a alterar a conta {string}', (nomeConta) => {   
    CriarContaPage.botaoEditar(nomeConta)
        
})

And('clico no botão salvar', () => {   
    CriarContaPage.botaoSalvar()
        
})

And('limpo o campo nome', () => {   
    CriarContaPage.limparNome()
        
})

When('alterar o nome da conta para {string}', (nomeContaAlterada) => {   
    CriarContaPage.nomeConta(nomeContaAlterada)
        
})

And('deve apresentar mensagem {string}', (mensagem) => {   
    CriarContaPage.mensagemAltContaSuc(mensagem)
        
})

Then('a conta deve apresentar no grid com o nome alterado {string}', (nome) => {   
    CriarContaPage.nomeContaAlterada(nome)
        
})

Then('deve apresentar a mensagem {string}', (mensagem) => {
    CriarContaPage.mensagemContaJaExiste(mensagem)
})

Given('Clico no botão Resetar', () => {   
    CriarContaPage.menuConta() 
    CriarContaPage.botaoResetar()
        
})


