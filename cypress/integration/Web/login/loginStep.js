/// <reference types="cypress" />

import { Given, When, Then} from 'cypress-cucumber-preprocessor/steps'
import LoginPage from '../../../support/Pages/Primeiro_Projeto/loginPage'


Given('Acessando a página de login', () => {    
        LoginPage.acessarSite()
        
})


And('Preencho o usuário e senha e entro no site', () => {       
        LoginPage.usuario()
        LoginPage.senha()
        LoginPage.botaoEntrar()
        LoginPage.bemVindo()
        
})

