/// <reference types="cypress" />

class MoviContaPage {

    //botão movimentação
    static botaoMovimentacao(){
        cy.get('[data-test=menu-movimentacao] > .fas').click()
    }

    // Campo descrição
    static campoDesc(desc){
        cy.get('#descricao').type(desc)
    }

    // Campos Valor
    static campoValor(valor){        
        cy.get('[data-test=valor]').type(valor)
    }

    // Campo Interessado
    static campoInteressado(descr){
        cy.get('[data-test=envolvido]').type(descr)
    }

    // Seleciona uma conta no Combobox COnta
    static comboboxConta(conta){
        cy.get('[data-test=conta]').select(conta)
    }

    //ícone de confirma a movimentação
    static confirmarMovi(){
        cy.get('[data-test=status]').click()
    }

    //Mensagem de movimentação inserida
    static msgMoviInserida(mensagem){
        cy.get('.toast-message').should('contain', mensagem )
        
    }

    // Botão Salvar da Tela de Movimentação
    static botaoSalvarTelaMov(){
        cy.get('.btn-primary').click()
    }

    // Home
    static home(){
        cy.get('[data-test=menu-home] > .fas').should('exist')
    }

    // Validando o valor de movimentação no GRID
    static validValorMov(valor){
        cy.xpath("//*[contains(text(),'" + valor + "')]").should('exist')
    }

    // Tela de movimentações
    static telaMovi(){
        cy.get('[data-test=menu-extrato] > .fas').click()
    }

    // Botão Excluir de um movimentação especifica
    static nomeMovi(nome){
        cy.xpath("//*[contains(text(),'" + nome + "')]/../../..//i[@class='far fa-trash-alt']").click() 
    }
    // Mensagem Movimentação Excluída com Sucesso
    static msgMovExcl(mensagem){
        cy.get('.toast-message').should('contain', mensagem )
    }

    

}

export default MoviContaPage;