/// <reference types="cypress" />

// Acessa o site que será testado
class CriarContaPage {
    

    //Menu Conta
    static menuConta() {
        cy.get('[data-test=menu-settings] > .fas').click()
        cy.get('[href="/contas"]').click()        
    }


    // Nome da conta e Salvar
    static nomeConta(nome) {
        cy.get('[data-test=nome]').type(nome)
        cy.get('.btn').click()

    }

    // Mensagem Conta Grava com Sucesso
    static mensagemContaGravada(mensagem){
        cy.get('.toast-message').should('contain.text', mensagem)
    }
    // Clicando no botão Reset do Menu de Contas
    static botaoResetar(){
        cy.get('[data-test=menu-settings] > .fas').click()
        cy.get('[href="/reset"]').click()
    }

    // limpar campo nome da tela de alteração
    static limparNome() {
        cy.get('[data-test=nome]').clear()
    }


    // Botão Editar de um conta especifica
    static botaoEditar(conta){
        cy.xpath("//*[contains(text(),'" + conta + "')]/..//i[@class='far fa-edit']").click() // estou passando o multiple: true por conta que na linha possui 2 ícones (excluir e editar)
    }

    // Botão Salvar
    static botaoSalvar() {
        cy.get('.btn').click()
    }    

    // mensagem alterada com sucesso
    static mensagemAltContaSuc(mensagem){
        cy.get('.toast-message').should('contain', mensagem )
    }

    // mensagem conta já existente
    static mensagemContaJaExiste(mensagem){
        cy.get('.toast-message').should('contain', mensagem )
    }

    // Validando nome da conta alterada no GRID
    static nomeContaAlterada(nome){
        cy.xpath("//*[contains(text(),'" + nome + "')]").should('exist')
    }


    
}

export default CriarContaPage ;
