/// <reference types="cypress" />

// Acessa o site que será testado
class LoginPage {
    
    static acessarSite() {
        cy.visit('http://barrigareact.wcaquino.me')
    }

    // Campo usuário
    static usuario() {
        cy.get('[data-test=email]').type('david.campos@zup.com.br')
    }

    // Campo senha
    static senha() {
        cy.get('[data-test=passwd]').type('teste')
    }

    // Botão Entrar
    static botaoEntrar () {
        cy.get('.btn').click()
    }
    
    // Mendagem de bem Vindo
    static bemVindo() {
        cy.get('.toast-message').should('contain', 'Bem vindo')
    }

}

export default LoginPage;
